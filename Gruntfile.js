module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      
      fonts: {
        expand: true,
        cwd: 'src/fonts/',
        src: '**',
        dest: 'www/fonts/'
      },
      imgs: {
        expand: true,
        cwd: 'src/img/',
        src: '**',
        dest: 'www/img/'
      },
      
      html: {
        expand: true,
        cwd: 'src/',
        src: '**/*.html',
        dest: 'www/'
      },
      /*
      js:{
        expand: true,
        cwd: 'src/js/',
        src: '**',
        dest: 'www/js/'
      }
      */
    },
    sprite:{
      all: {
        src: 'src/sprites/*.png',
        dest: 'www/img/otherSpritesheet.png',
        destCss: 'src/css/otherSprites.css'
      },
      categoryBigSprites:{
        src: 'src/sprites/categoryBig/*.png',
        dest: 'www/img/categoryBig.png',
        destCss: 'src/css/categoryBig.css'
      },
      categorySmallSprites:{
        src: 'src/sprites/categorySmall/*.png',
        dest: 'www/img/categorySmall.png',
        destCss: 'src/css/categorySmall.css'
      }
    },
    cssmin: {
      target: {
        files: {
          'www/css/style.css': ['src/css/fonts.css','src/css/sprites.css', 'src/css/style.css']
        }
      }
    },

    watch: {
        html: {
          files: 'src/*.html',
          tasks: 'copy:html'
        },
        css: {
          files: 'src/css/*.css',
          tasks: 'cssmin'
        },
        sprite:{
          files: 'src/sprites/*.png',
          tasks: 'sprite'
        },
        imgs:{
          files: 'src/img/*',
          tasks: 'copy:imgs'
        }
      }

    });

  // Load the plugin that provides the "uglify" task.
  //grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-spritesmith');

  // Default task(s).
  grunt.registerTask('default', ['copy','sprite','cssmin']);

};